import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Header } from 'react-native-elements';
import { AntDesign } from '@expo/vector-icons';

import EditScreenInfo from '../components/EditScreenInfo';
import { View } from '../components/Themed';
import { useNavigation } from '@react-navigation/native';

export default function TabTwoScreen() {
    const navigation = useNavigation();
  return (
    <View style={styles.container}>
        <Header
            leftComponent={
                <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                    <AntDesign name="arrowleft" size={24} color="black" />
                </TouchableOpacity>
            }
            containerStyle={{
                backgroundColor: '#FFF',
                justifyContent: 'space-around',
             }}
            
            rightComponent={{ icon: 'home', color: '#fff' }}
        />
      <Text style={styles.title}>Tab Two</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <EditScreenInfo path="/screens/TabTwoScreen.tsx" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
