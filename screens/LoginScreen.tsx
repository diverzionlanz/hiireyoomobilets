import React, {useRef, useState} from 'react';
import { Provider } from 'react-native-paper'
import { View, Image, Text, StyleSheet, TouchableOpacity, ViewStyle, TextStyle, StyleProp,  ImageStyle, Platform} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import RoundedButton from '../components/RoundedButton';
import AppIntroSlider from "react-native-app-intro-slider";
import { RootStackScreenProps } from '../types';
import Background from '../components/Background'
import LogoCenter from '../components/LogoCenter'
import TextInput from '../components/TextInput'
import TextPassword from '../components/TextPassword'
import CheckBox from 'react-native-check-box'
import GreenButton from '../components/GreenButton'
import HeaderBackground from '../assets/svg/hero-background'
import { FacebookSocialButton, GoogleSocialButton } from "react-native-social-buttons";
import HomeIconSVG from "../assets/svg/home"
import Logo from "../components/Logo"
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


interface Styles {
    forgotpasswords: ViewStyle;
    row: ViewStyle;
    label: ViewStyle;
    RememberPasswordLabel: TextStyle;
    forgot: ViewStyle;
    container: ViewStyle;
    checkboxContainer: ViewStyle;
    greenbutton: ViewStyle;
    signinLabel: TextStyle;
    createAccountRow: ViewStyle;
    LogoPosition: ViewStyle;
  
  }

const Login = ({ navigation }: RootStackScreenProps<'Dashboard'>) => {
    const pagerRef = useRef(null);
    const [done,setDone] = useState(false)
    const [email, setEmail] = useState('')

    const HandleLogin = () => {
        navigation.reset({
            index: 0,
            routes: [{ name: 'Dashboard' }],
          })
    }

    return (
        <Background >
            
            <HeaderBackground />
            
            <View style={{flex: 1, position: 'absolute', width: '100%'}}>
                <View style={styles.LogoPosition}>
                   <Logo width={150} height={hp('10%')} Iconcolor="#ffffff" Textcolor="#ffffff"/>
                </View>
                <View style={{marginTop: hp('20%')}}>
                    <View style={styles.label}>
                        <Text style={styles.forgot}>Email Address</Text>
                    </View>
                    <TextInput/>
                    <View style={styles.label}>
                       <Text style={styles.forgot}>Password</Text>
                    </View>
                </View>
                <TextPassword/>
                <View style={styles.checkboxContainer}>
                        <CheckBox
                        style={{flex: 1, padding: 10}}
                        rightText={"Remember Password"}
                        isChecked={false}
                    />
                   <Text style={styles.RememberPasswordLabel}>Forgot Password</Text>
                </View>
                <TouchableOpacity style={styles.greenbutton} onPress={() => HandleLogin()}>
                    <Text style={styles.signinLabel}>Sign In</Text>
                </TouchableOpacity>
                <View style={{alignItems: 'center', paddingTop: 20}}>
                   <Text>Or</Text>
                </View>
                <FacebookSocialButton buttonViewStyle={{borderRadius:15, height: 46,  marginTop: 20, width: "100%"}} />
                <GoogleSocialButton  buttonViewStyle={{borderRadius:15, height: 46, borderColor: "#000000", marginTop: 20, width: "100%"}} />
                <View style={{alignItems: "center"}}>
                    <View style={styles.createAccountRow}>
                        <Text>New to Hireyoo? </Text>
                        <TouchableOpacity><Text>Create an Account</Text></TouchableOpacity>
            
                    </View>
                </View>
                
            </View>
            
          
            
        </Background>
  );
};

export default Login;

const styles = StyleSheet.create<Styles>({
    forgotpasswords: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    label: {
        width: '100%',
        alignItems: 'flex-start',
        marginBottom: 9,
    },
    RememberPasswordLabel: {
        alignItems:'flex-end',
        fontSize: 12,
        marginTop: 12,
    },

    LogoPosition: {
      flex: 1, 
      marginLeft: wp('20%'), 
      position: 'absolute', 
      // top: hp('-3%')
      top: Platform.OS === 'ios' ? hp('-3%') : hp('3%')
    },
    
    forgot: {
        fontSize: 13,
        alignItems: 'flex-end',
        marginTop: 20,
        color: '#000000',
      },
      container: {
        flex: 1,
        padding: 1,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
      },
      checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
        alignItems: 'flex-start',
      },
      greenbutton: {
        borderRadius: 20,
        backgroundColor: '#0097a9',
        alignItems: 'center', 
        paddingBottom: hp('2%'),
        paddingTop: hp('2%'),
      },
      signinLabel: {
        color: "#ffffff",
        fontWeight: "800",
        fontSize: 20,
      },
      createAccountRow: {
        flexDirection: "row",
        fontWeight: "800",
        alignItems: 'center', 
        marginTop: 10, 
      }


});    


// resources : https://www.npmjs.com/package/react-native-app-intro-slider