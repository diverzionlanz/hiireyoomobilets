
import React from "react";
import { Text, View, Pressable, ColorSchemeName, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons, FontAwesome, Feather } from '@expo/vector-icons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import SVGHomeIcon from '../assets/svg/home'
import SVGSearchIcon from '../assets/svg/search'
import SVGPostIcon from '../assets/svg/post'
import SVGNotificationIcon from '../assets/svg/notification'
import SVGApplicationIcon from '../assets/svg/application'
import ModalScreen from './ModalScreen';
import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import NotificationScreen from './NotificationScreen';
import ApplicationScreen from './ApplicationScreen';
import ProfileImage from '../components/ProfileImage'
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import { useNavigation } from '@react-navigation/native';
const image = { uri: "https://reactjs.org/logo-og.png" };

import { createDrawerNavigator } from "@react-navigation/drawer";

// import { ContactStackNavigator } from "./StackNavigator";
import TabNavigator from "./DashboardScreen";
import SearchStack from '../screens/SearchStackScreen'
import NotificationStack from '../screens/NotificationStackScreen'
import ApplicationStack from '../screens/ApplicationStack'
import Login from "./LoginScreen"

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  const colorScheme = useColorScheme();
  const Callnavigation = useNavigation();
  const modal = () => {
    Callnavigation.navigate('Modal');
  }
  return (
    <Drawer.Navigator

    screenOptions={({ route }) => ({
        
        headerRight: () => (
          <TouchableOpacity  onPress={() => Callnavigation.navigate('Profile')}>
              {/* <Ionicons name="menu" size={24} color="black" style={{ marginLeft: 15 }}  /> */}
              <ProfileImage  image={image} height={30} width={30}/>
           </TouchableOpacity>
        ),
        
        
        tabBarActiveTintColor: '#0097a7',
        tabBarInactiveTintColor: 'gray',
      })}
    
    >
      <Drawer.Screen name="Home" component={TabNavigator}  />
      <Drawer.Screen name="Search" component={SearchScreen}  />
      <Drawer.Screen name="Notification" component={NotificationStack }  />
      <Drawer.Screen name="Application" component={ApplicationStack } />
    </Drawer.Navigator>
  );
}

export default DrawerNavigator;


// resources: https://dev.to/deversity/combining-drawer-tab-and-stack-navigators-in-react-navigation-6-l4m