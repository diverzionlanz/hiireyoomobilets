import { StyleSheet, TouchableOpacity, Text,  ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { AntDesign, Entypo, Feather, MaterialIcons } from '@expo/vector-icons';

import EditScreenInfo from '../components/EditScreenInfo';
import { View } from '../components/Themed';
import { useNavigation } from '@react-navigation/native';
import ProfileImage from '../components/ProfileImage'
const image = { uri: "https://reactjs.org/logo-og.png" };
import Logo from "../components/Logo"
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


export default function TabTwoScreen() {
    const navigation = useNavigation();

    const HandleLogout = () => {
      navigation.reset({
          index: 0,
          routes: [{ name: 'Login' }],
        })
    }

  return (
    <View style={styles.container}>
        <Header
            leftComponent={
                <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
                    <AntDesign name="arrowleft" size={24} color="black" />
                </TouchableOpacity>
            }
            containerStyle={{
                backgroundColor: '#FFF',
                justifyContent: 'space-around',
             }}
            centerComponent={{ text: 'Company Profile', style: { color: '#000000', fontSize: 20 } }}
            rightComponent={{ icon: 'home', color: '#fff' }}
        />
      <TouchableOpacity onPress={() => navigation.navigate('AboutUsDetails')}>
          <View style={{flexDirection: 'row', paddingLeft: 20, marginTop: 20}}>
              <ProfileImage  image={image} height={45} width={45}/>
              <View>
                  <Text style={styles.companyNameLabel}>Hireyoo</Text>
                 <Text style={styles.viewProfileLabel}>View Profile</Text>
              </View>
          </View>
      </TouchableOpacity>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <ScrollView>
      <TouchableOpacity onPress={() => navigation.navigate('AboutUs')}>
          <View style={{flexDirection: 'row', paddingLeft: 20}}>
              <View style={{flexDirection: 'row', width: wp('80%')}}>
                  <Entypo name="info-with-circle" size={24} color="black" />
                  <Text style={styles.label}>About Us</Text>
              </View>
              <View style={{width:wp('20%')}}>
                  <Feather name="arrow-right" size={24} color="black" />
              </View>
            
          </View>
      </TouchableOpacity>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20}}>
          <View style={{flexDirection: 'row', width: wp('80%')}}>
              <AntDesign name="clockcircleo" size={24} color="black" />
              <Text style={styles.label}>Working Hours</Text>
          </View>
          <View style={{width:wp('20%')}}>
               <TouchableOpacity>
                   <Feather name="arrow-right" size={24} color="black" />
               </TouchableOpacity>

          </View>
         
      </View>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20}}>
          <View style={{flexDirection: 'row', width: wp('80%')}}>
              <AntDesign name="checkcircleo" size={24} color="black" />
              <Text style={styles.label}>Benefits</Text>
          </View>
          <View style={{width:wp('20%')}}>
               <TouchableOpacity>
                   <Feather name="arrow-right" size={24} color="black" />
               </TouchableOpacity>

          </View>
         
      </View>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20}}>
          <View style={{flexDirection: 'row', width: wp('80%')}}>
              <Feather name="globe" size={24} color="black" />
              <Text style={styles.label}>Website</Text>
          </View>
          <View style={{width:wp('20%')}}>
               <TouchableOpacity>
                   <Feather name="arrow-right" size={24} color="black" />
               </TouchableOpacity>

          </View>
         
      </View>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20}}>
          <View style={{flexDirection: 'row', width: wp('80%')}}>
              <AntDesign name="staro" size={24} color="black" />
              <Text style={styles.label}>Subscription</Text>
          </View>
          <View style={{width:wp('20%')}}>
               <TouchableOpacity>
                   <Feather name="arrow-right" size={24} color="black" />
               </TouchableOpacity>

          </View>
         
      </View>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20}}>
          <View style={{flexDirection: 'row', width: wp('80%')}}>
              <Feather name="settings" size={24} color="black" />
              <Text style={styles.label}>Settings</Text>
          </View>
          <View style={{width:wp('20%')}}>
               <TouchableOpacity>
                   <Feather name="arrow-right" size={24} color="black" />
               </TouchableOpacity>

          </View>
         
      </View>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <View style={{flexDirection: 'row', paddingLeft: 20, marginBottom: 100}}>
          <TouchableOpacity onPress={() => HandleLogout()}>
            <View style={{flexDirection: 'row', width: wp('80%')}}>
                <MaterialIcons name="logout" size={24} color="black" />
                <Text style={styles.label}>Logout</Text>
            </View>
          </TouchableOpacity>     
      </View>
      </ScrollView>
      {/* <EditScreenInfo path="/screens/TabTwoScreen.tsx" /> */}
      
     
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  label: {
    fontSize: 20,
    marginLeft: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '100%',
  },
  viewProfileLabel: {
    fontSize: 12
  },
  companyNameLabel: {
    fontSize: 18,
    fontWeight: '500',
    marginTop: -4,
  }
});
