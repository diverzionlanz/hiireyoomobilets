import * as React from 'react';
import { Text, View, Pressable, ColorSchemeName, StyleSheet, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons, FontAwesome, Feather } from '@expo/vector-icons';
import { RootTabScreenProps } from '../types';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import Notifications from '../components/Notifications'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';

export default function NotificationScreen({ navigation }: RootTabScreenProps<'HomeScreen'>) {
    return (
        
            <View style={styles.container}>
                <View style={styles.row}>
                   <Text style={styles.greetings}>Your notifications</Text>
                   <TouchableOpacity onPress={() => navigation.toggleDrawer()}><Text>sdsd</Text></TouchableOpacity>
                </View>
                <Notifications />
            </View>
      
    );

      
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    breadCrumb: {
        fontSize: 20,
    },
    greetings: { 
       paddingTop: hp('1%'),
       fontSize: hp('3%'),
       fontWeight: '800'
    },
    row: {
        flexDirection: "column",
        paddingLeft: wp('7%'),
        paddingTop: wp('10%'),
        paddingBottom: hp('2%'),
        backgroundColor: '#FFFFFF'
       
    },
    searchInputRow: {
        flexDirection: "row",
        
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    separator: {
      marginVertical: 30,
      height: 1,
      width: '80%',
    },
  });
