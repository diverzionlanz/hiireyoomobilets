import { StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import { RootTabScreenProps } from '../types';
import SearchInput from '../components/SearchInput';
import SearchLocation from '../components/SearchLocation';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import React from 'react';


export default function SearchScreen({ navigation }: RootTabScreenProps<'HomeScreen'>) {
  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>HomeScreen</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <EditScreenInfo path="/screens/TabOneScreen.tsx" /> */}
      <View style={styles.row}>
         <Text style={styles.breadCrumb}>What</Text>
         <Text>Job title, keywords, or company</Text>
      </View>
      <View style={styles.searchInputRow}>
          <SearchInput />
          
      </View>
      <View style={styles.row}>
         <Text style={styles.breadCrumb}>Where</Text>
         <Text>Job title, keywords, or company</Text>
      </View>
      <View style={styles.searchInputRow}>
          <SearchLocation />
          
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.greenbutton}>
            <Text style={styles.signinLabel}>Search</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
         <Text style={styles.breadCrumb}>Recent searches</Text>
      </View>
      <View style={styles.row}>
         <Text>UI/UX Designer</Text>
         <Text>15 new in Cebu City, Cebu</Text>
      </View>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  breadCrumb: {
      fontSize: 20,
  },
  greenbutton: {
    borderRadius: 20,
    backgroundColor: '#0097a9',
    alignItems: 'center', 
    paddingBottom: hp('2%'),
    paddingTop: hp('2%'),
    width: wp('87%')
  },
  signinLabel: {
    color: "#ffffff",
    fontWeight: "800",
    fontSize: 20,
  },
  greetings: { 
     paddingTop: hp('1%'),
     fontSize: hp('3%'),
     fontWeight: '800'
  },
  row: {
      flexDirection: "column",
      paddingLeft: wp('7%'),
      paddingTop: wp('10%'),
  },
  searchInputRow: {
      flexDirection: "row",
      
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
