import { StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import Category from '../components/Category'
import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import SearchInput from '../components/SearchInput';
import FeaturedJobPost from '../components/FeaturedJobPost'
import PopularPost from '../components/PopularJobPost'
import FeaturedApplicants from '../components/FeaturedApplicants';
import OtherJobPost from '../components/OtherJobPost';
import OutSourceJobPost from '../components/OutSourceJobPost';
import LatestJobPost from '../components/LatestJobPost';
import { RootTabScreenProps } from '../types';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
import React from 'react';


export default function ApplicationScreen({ navigation }: RootTabScreenProps<'HomeScreen'>) {
  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>HomeScreen</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <EditScreenInfo path="/screens/TabOneScreen.tsx" /> */}
      <View style={styles.row}>
         <Text style={styles.breadCrumb}>Your job applications</Text>

      </View>
      <ScrollView>
        <LatestJobPost />
        <OtherJobPost />
        <OutSourceJobPost />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  breadCrumb: {
      fontSize: 20,
  },
  greetings: { 
     paddingTop: hp('1%'),
     fontSize: hp('3%'),
     fontWeight: '800'
  },
  row: {
      flexDirection: "column",
      paddingLeft: wp('7%'),
      paddingTop: wp('10%'),
  },
  searchInputRow: {
      flexDirection: "row",
      
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
