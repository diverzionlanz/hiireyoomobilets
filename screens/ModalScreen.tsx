import { StatusBar } from 'expo-status-bar';
import { Platform, StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import CustomDropdown from '../components/CustomDropdown';
import MultiDropDown from '../components/MultiDropDown';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default function ModalScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.row}>
          <Text style={styles.label}>Category</Text>
          <CustomDropdown />
      </View>
      <View style={styles.row}>
          <Text style={styles.label}>Sub Category</Text>
          <CustomDropdown />
      </View>
      <View style={styles.row}>
          <Text style={styles.label}>Location</Text>
          <CustomDropdown />
      </View>
      <View style={styles.row}>
          <Text style={styles.label}>Job Type</Text>
          <MultiDropDown />
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.greenbutton}>
            <Text style={styles.signinLabel}>Apply Filters</Text>
        </TouchableOpacity>
      </View>
      
      {/* <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" /> */}
      {/* <EditScreenInfo path="/screens/ModalScreen.tsx" /> */}

      {/* Use a light status bar on iOS to account for the black space above the modal */}
      <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: hp('4%'),
    paddingLeft: wp('5%')
  },
  row: {
    marginBottom: 30,
    width: wp('90%'),
  },
  label: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  greenbutton: {
    borderRadius: 20,
    backgroundColor: '#0097a9',
    alignItems: 'center', 
    paddingBottom: hp('2%'),
    paddingTop: hp('2%'),
  },
  signinLabel: {
    color: "#ffffff",
    fontWeight: "800",
    fontSize: 20,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
