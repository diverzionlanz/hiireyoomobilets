import * as React from 'react';
import { Text, View, Pressable, ColorSchemeName, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons, FontAwesome, Feather } from '@expo/vector-icons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import SVGHomeIcon from '../assets/svg/home'
import SVGSearchIcon from '../assets/svg/search'
import SVGPostIcon from '../assets/svg/post'
import SVGNotificationIcon from '../assets/svg/notification'
import SVGApplicationIcon from '../assets/svg/application'
import ModalScreen from './ModalScreen';
import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import NotificationScreen from './NotificationScreen';
import ApplicationScreen from './ApplicationScreen';
import ProfileImage from '../components/ProfileImage'
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import { useNavigation } from '@react-navigation/native';
const image = { uri: "https://reactjs.org/logo-og.png" };

function PostScreen(){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Post</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function App({ navigation }: RootTabScreenProps<'SearchScreen'>) {
  const colorScheme = useColorScheme();
  const Callnavigation = useNavigation();
  const modal = () => {
    navigation.navigate('Modal');
  }
  return (

      <Tab.Navigator
      initialRouteName="Notification"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          let select;

          if (route.name === 'Home') {
            iconName = focused
              ? select = 'selected'
              : select = '';
              return <SVGHomeIcon status={select} />
              
          } else if (route.name === 'Search') {
            iconName = focused ?  select = 'selected' : select = '';
            return <SVGSearchIcon status={select} />
          }  else if (route.name === 'Post') {
            iconName = focused ?  select = 'selected' : select = '';
            return <SVGPostIcon status={select} />
          } else if (route.name === 'Notification') {
            iconName = focused ?  select = 'selected' : select = '';
            return <SVGNotificationIcon status={select} />
          } else if(route.name === 'Application') {
            iconName = focused ?  select = 'selected' : select = '';
            return <SVGApplicationIcon status={select} />
          }

          // You can return any component that you like here!
          // return <Ionicons name={iconName} size={size} color={color} />;
          // return <SVGHomeIcon status={} />
        },
        tabBarActiveTintColor: '#0097a7',
        tabBarInactiveTintColor: 'gray',
      })}
      >
        <Tab.Screen name="Home" component={HomeScreen}  />
        <Tab.Screen name="Search" component={SearchScreen} />
        <Tab.Screen name="Post" component={PostScreen} />
        <Tab.Screen name="Notification" component={NotificationScreen} />
        <Tab.Screen name="Application" component={ApplicationScreen }  />
      </Tab.Navigator>

  );
}