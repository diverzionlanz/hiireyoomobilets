import React, {useRef, useState} from 'react';
import { Provider } from 'react-native-paper'
import { ImageBackground, StyleSheet, View, Text, Image, KeyboardAvoidingView, ViewStyle, TextStyle, StyleProp,  ImageStyle } from 'react-native'
import ViewPager from '@react-native-community/viewpager';
import RoundedButton from '../components/RoundedButton';
import AppIntroSlider from "react-native-app-intro-slider";
import SvgSlider from "../components/SliderSvg"
import { RootStackScreenProps } from '../types';
import Logo from '../components/Logo'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

interface Styles {
  image: ViewStyle;

}


const Onboarding = ({ navigation }: RootStackScreenProps<'Onboarding'>) => {
    const pagerRef = useRef(null);
    const [done,setDone] = useState(false)

    const _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        
        navigation.navigate('Login')
      }

    const slides = [
        {
          key: "one",
          title: "",
          slide: 1,
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide1.svg"),
        },
        {
          key: "two",
          title: "",
          // image: <Slide1 style={styles.image} width={200} height={hp('10%')} />,
          slide: 2,
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide2.svg"),
        },
        {
          key: "three",
          title: "",
          slide: 3,
          // image: <Slide1 style={styles.image} width={200} height={hp('10%')} />,
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide3.svg"),
        },
      ];

      const _renderDoneButton = () => {
        return (
          <View>
            <Text>Done</Text>
          </View>
        );
      };

      const _renderPrevButton = () => {
        return (
          <View>
            <Text>Previous</Text>
          </View>
        );
      };

      const _renderNextButton = () => {
        return (
          <View>
            <Text>Next</Text>
          </View>
        );
      };

      const _renderItem = ({ item }:any) => {
        return (
          <View style={{ flex: 1 }}>
             <Text
              style={{
                paddingTop: 25,
                paddingBottom: 10,
                fontSize: 23,
                fontWeight: "bold",
                color: "#21465b",
                alignSelf: "center",
              }}
            >
              {item.title}
            </Text>
            {/* <Image
              source={item.image}
              style={{
                resizeMode: "cover",
                height: "50%",
                width: "100%",
              }}
            /> */}
            <SvgSlider slideNo = {item.slide} />

    
            <Text style={{
              textAlign:"center",
              color:"#b5b5b5",
              fontSize:15,
              paddingHorizontal:30
            }}>
              {item.text}
            </Text>
          </View>
        );
      };

    
    const handlePageChange = (number: number) => {
        //  pagerRef.current.setPage(()=>{number+1})
        return true
      };
    return (
        <View style={{ flex: 1, paddingTop: hp('6%') }}>
            <View style={{paddingLeft: hp('2%') }}>
                <Logo width={140} height={hp('10%')} Iconcolor="#0097a9" Textcolor="#0d3956"/>
            </View>

            <AppIntroSlider
                renderItem={_renderItem} 
                data={slides} 
                activeDotStyle={{
                    backgroundColor:"#21465b",
                    width:30
                }}
                renderDoneButton={_renderDoneButton}
                showPrevButton
                showNextButton
                renderPrevButton={_renderPrevButton}
                renderNextButton={_renderNextButton}
                onDone={_onDone}
            />
        </View>
  );
};

const styles = StyleSheet.create<Styles>({
  image: {
    width: 300,
    height: hp('2%'),
    marginTop: hp('1%'),
    marginBottom: hp('3%'),
  },
});



export default Onboarding;



// resources : https://www.npmjs.com/package/react-native-app-intro-slider