import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native'
import 'react-native-gesture-handler';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from './types';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import OnboardingScreen from './screens/OnboardingScreen';
import Modal from './screens/ModalScreen';
import LoginScreen from './screens/LoginScreen';
import LinkingConfiguration from './navigation/LinkingConfiguration';
// import DashboardScreen from './screens/DashboardScreen';
import ProfileScreen from './screens/ProfileScreen';
import AboutUsScreen from './screens/AboutUsScreen'
import AboutUsDetailsScreen from './screens/AboutUsDetailScreen'
import DashboardScreen from './screens/DrawerNavigator'
export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();
  const [login, setLogin] = useState(false)

  const Stack = createNativeStackNavigator<RootStackParamList>();

  const storeData = async (value:any) => {
    try {
      await AsyncStorage.setItem('@storage_Key', value)
    } catch (e) {
      // saving error
    }
  }

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        {/* <Onboarding />
        <Navigation colorScheme={colorScheme} /> */}
          
        <NavigationContainer>
              <Stack.Navigator>
                  <Stack.Screen name="Onboarding" component={OnboardingScreen} options={{ headerShown: false }} />
                  <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
                  <Stack.Screen name="Dashboard" component={DashboardScreen} options={{  headerShown: false }} />
                  <Stack.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false }} />
                  <Stack.Screen name="AboutUs" component={AboutUsScreen} options={{ headerShown: false }} />
                  <Stack.Screen name="AboutUsDetails" component={AboutUsDetailsScreen} options={{ headerShown: false }} />
                  <Stack.Group screenOptions={{ presentation: 'modal' }}>
                      <Stack.Screen name="Filters" component={Modal} />
                  </Stack.Group>
              </Stack.Navigator>
        </NavigationContainer>
        <StatusBar />
      </SafeAreaProvider>
    );
  
}
}