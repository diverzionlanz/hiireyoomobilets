import React, {useRef, useState} from 'react';
import { View, Image, Text } from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import RoundedButton from '../components/RoundedButton';
import AppIntroSlider from "react-native-app-intro-slider";

import Page from '../components/page';
import Footer from './footer'


const Onboarding = () => {
    const pagerRef = useRef(null);
    const [done,setDone] = useState(false)

    const _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        setDone(true)
        console.log(done)
      }

    const slides = [
        {
          key: "one",
          title: "JUST TRAVEL",
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide1.svg"),
        },
        {
          key: "two",
          title: "TAKE A BREAK",
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide2.svg"),
        },
        {
          key: "three",
          title: "ENJOY YOUR JOURNEY",
          text:
            "Lorem ipsum dolor sit amet consecte tuer adipsing elit sed diam monum my nibh eusimod eltor",
        //   image: require("../assets/svg/slide3.svg"),
        },
      ];

      const _renderDoneButton = () => {
        return (
          <View>
            <Text>Done</Text>
          </View>
        );
      };

      const _renderPrevButton = () => {
        return (
          <View>
            <Text>Previous</Text>
          </View>
        );
      };

      const _renderNextButton = () => {
        return (
          <View>
            <Text>Next</Text>
          </View>
        );
      };

      const _renderItem = ({ item }:any) => {
        return (
          <View style={{ flex: 1 }}>
            <Image
              source={item.image}
              style={{
                resizeMode: "cover",
                height: "73%",
                width: "100%",
              }}
            />
            <Text
              style={{
                paddingTop: 25,
                paddingBottom: 10,
                fontSize: 23,
                fontWeight: "bold",
                color: "#21465b",
                alignSelf: "center",
              }}
            >
              {item.title}
            </Text>
    
            <Text style={{
              textAlign:"center",
              color:"#b5b5b5",
              fontSize:15,
              paddingHorizontal:30
            }}>
              {item.text}
            </Text>
          </View>
        );
      };

    
    const handlePageChange = (number: number) => {
        //  pagerRef.current.setPage(()=>{number+1})
        return true
      };
    return (
        <View style={{ flex: 1 }}>
           
            <AppIntroSlider
                renderItem={_renderItem} 
                data={slides} 
                activeDotStyle={{
                    backgroundColor:"#21465b",
                    width:30
                }}
                renderDoneButton={_renderDoneButton}
                showPrevButton
                showNextButton
                renderPrevButton={_renderPrevButton}
                renderNextButton={_renderNextButton}
                onDone={_onDone}
            />
        </View>
  );
};

export default Onboarding;

// resources : https://www.npmjs.com/package/react-native-app-intro-slider