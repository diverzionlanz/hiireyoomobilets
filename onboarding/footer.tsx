import React from 'react';
import { View, useWindowDimensions } from 'react-native';

import RoundedButton from '../components/RoundedButton'

interface Props {
    backgroundColor: string,
    rightButtonLabel: string,
    rightButtonPress: any,
}


const Footer = ({
  backgroundColor,
  rightButtonLabel,
  rightButtonPress
}:Props) => {
  const windowWidth = useWindowDimensions().width;
  const HEIGHT = windowWidth * 0.21;
  const FOOTER_PADDING = windowWidth * 0.1;

  console.log(rightButtonLabel)

  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        height: HEIGHT,
        backgroundColor,
        opacity: 0.6,
        alignItems: 'center',
        paddingHorizontal: FOOTER_PADDING
      }}
    >
      <RoundedButton label={rightButtonLabel} press={rightButtonPress} />
    </View>
  );
};

export default Footer;