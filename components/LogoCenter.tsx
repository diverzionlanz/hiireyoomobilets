import React from 'react'
import { ImageBackground, StyleSheet, KeyboardAvoidingView, ViewStyle, TextStyle, StyleProp,  ImageStyle } from 'react-native'
import { View, Image, Text } from 'react-native';
import SVGLogo from '../assets/svg/logo'
import SVGSlide from '../assets/svg/slide1'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


interface Styles {
    image: ViewStyle;
  
}


const LogoCenter = () => {
    return (
       <SVGLogo style={styles.image} width={200} height={hp('10%')} ></SVGLogo>
      );
}


const styles = StyleSheet.create<Styles>({
    image: {
      width: 300,
      height: hp('2%'),
      marginTop: hp('1%'),
      marginBottom: hp('3%'),
    },
});

export default LogoCenter;