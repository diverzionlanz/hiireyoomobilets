import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import { Hoverable, Pressable, } from 'react-native-web-hover'
import { Ionicons, FontAwesome, Feather, AntDesign } from '@expo/vector-icons';
import AppIcon from '../components/appicon'
const image = { uri: "https://reactjs.org/logo-og.png" };
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ListItem = ({ item }:any) => {
  const [bg, setbg] = useState('#f2f3f3');
  const [positionBg, setPositionBg] = useState('#ffa443');
  const [textColor, setTextColor] = useState('#000000');
  const [iconColor, setIconColor] = useState('#000000');
  const [heartColor, setHeartColor] = useState('#ffffff');
  return (
    
      <View style={styles.item}>
        <Image
          source={{
            uri: item.uri,
          }}
          style={styles.itemPhoto}
          resizeMode="cover"
        ></Image>
    
            {/* <Text style={styles.itemText}>{item.text}</Text> */}
            <View style={{position: "absolute", top: 1, flexDirection: 'row', height: hp('30%')}}>
                  
                 <View style={{width: '80%'}}>
                     <AppIcon image={image} height={2} width={2} />
                 </View>
                 <View style={{width: '20%', marginTop: 10}}>
                      <TouchableOpacity onPress={()=>{setbg('#013954'), setPositionBg('#0097a7'), setTextColor('#ffffff'), setIconColor('#ffffff'), setHeartColor('#0097a7')}}>
                          <AntDesign name="heart" size={24} color={heartColor} />
                      </TouchableOpacity>
               </View>
            </View>
            <View style={{position: "absolute", flexDirection: 'row', top: 70, paddingBottom: hp('3%'), width: wp('49.9%'), borderBottomRightRadius: 20, borderBottomLeftRadius: 20, height: hp('11%'), backgroundColor: bg, paddingLeft: 10, paddingRight: 10, paddingTop: 10}}>
               <View style={{width: wp('35%')}}>
                   <Text style={{color: textColor, marginBottom: 6}}>Hireyoo</Text>
                   <View style={{backgroundColor: positionBg, padding: 5}}><Text style={{color: '#ffffff', fontSize: hp('1.5%')}}>Full Stack Developer</Text></View>
                   <Text style={{color: textColor, fontSize: 10}}>58k - Cebu City, Philippines</Text>
                   
               </View>
               <View style={{width: '20%'}}>
                  <AntDesign name="ellipsis1" size={30} style={{marginTop:-6}} color={iconColor} />
               </View>
                
            </View>
      
        {/* <Text style={styles.itemText}>{item.text}</Text> */}
      </View>
    
  );
};

export default () => {
  return (
    <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
           <Text style={styles.title}>Featured Job Post</Text>
           <TouchableOpacity><Text style={{textAlign:'right'}}>View More</Text></TouchableOpacity>
        </View>
        <SectionList
          horizontal
          contentContainerStyle={{ paddingHorizontal: 10 }}
          stickySectionHeadersEnabled={false}
          sections={SECTIONS}
       
          renderItem={({ item, section }) => {
            return <ListItem item={item} />;
            
          }}
          showsHorizontalScrollIndicator={false}
        />
    </View>
  );
};
const SECTIONS = [
    {
      title: 'Made for you',
      data: [
        {
          key: '1',
          text: 'Item text 1',
          uri: 'https://picsum.photos/id/1/200',
        },
        {
          key: '2',
          text: 'Item text 2',
          uri: 'https://picsum.photos/id/10/200',
        },
  
        {
          key: '3',
          text: 'Item text 3',
          uri: 'https://picsum.photos/id/1002/200',
        },
        {
          key: '4',
          text: 'Item text 4',
          uri: 'https://picsum.photos/id/1006/200',
        },
        {
          key: '5',
          text: 'Item text 5',
          uri: 'https://picsum.photos/id/1008/200',
        },
           
      ],
    },
    {
      title: 'Punk and hardcore',
      data: [
        {
          key: '1',
          text: 'Item text 1',
          uri: 'https://picsum.photos/id/1011/200',
        },
        {
          key: '2',
          text: 'Item text 2',
          uri: 'https://picsum.photos/id/1012/200',
        },
  
        {
          key: '3',
          text: 'Item text 3',
          uri: 'https://picsum.photos/id/1013/200',
        },
        {
          key: '4',
          text: 'Item text 4',
          uri: 'https://picsum.photos/id/1015/200',
        },
        {
          key: '5',
          text: 'Item text 5',
          uri: 'https://picsum.photos/id/1016/200',
        },
      ],
    },
    {
      title: 'Based on your recent listening',
      data: [
        {
          key: '1',
          text: 'Item text 1',
          uri: 'https://picsum.photos/id/1020/200',
        },
        {
          key: '2',
          text: 'Item text 2',
          uri: 'https://picsum.photos/id/1024/200',
        },
  
        {
          key: '3',
          text: 'Item text 3',
          uri: 'https://picsum.photos/id/1027/200',
        },
        {
          key: '4',
          text: 'Item text 4',
          uri: 'https://picsum.photos/id/1035/200',
        },
        {
          key: '5',
          text: 'Item text 5',
          uri: 'https://picsum.photos/id/1038/200',
        },
      ],
    },
  ];
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 35,
  },
  title: {
      fontWeight: '700',
      marginLeft: 20,
      fontSize: 15,
      width: wp('70%')
  },
  sectionHeader: {
    fontWeight: '800',
    fontSize: 18,
    color: '#f4f4f4',
    marginTop: 20,
    marginBottom: 5,
  },
  item: {
    margin: 10,
    width: wp('50%'),
    height: hp('21%'),
    borderRadius: 20,
  },
  itemPhoto: {
    width: wp('50%'),
    height: hp('15%'),
    borderRadius: 20,
  },
  itemText: {
    color: '#000000',
    marginTop: 5,
  },
  code: {
    fontFamily: "Arial"
  },
  link: {
    color: "blue"
  },
  linkHover: {
    textDecorationLine: "underline"
  }
});
