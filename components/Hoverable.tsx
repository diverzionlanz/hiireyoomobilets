import { Hoverable } from 'react-native-hoverable';
import * as React from 'react'
import {View} from 'react-native'

const HoverableView = () => (
  <Hoverable
    onMouseEnter={() => console.log('onMouseEnter')}
    onMouseLeave={() => console.log('onMouseLeave')}
    onMouseMove={() => console.log('onMouseMove')}
    style={({ hovered }) => [
      { padding: hovered ? 20 : 0 },
      { backgroundColor: 'purple' },
    ]}
  >
    {({ hovered }) => (
      <View
        style={{
          backgroundColor: hovered ? 'red' : 'green',
          height: 100,
          width: 100,
        }}
      />
    )}
  </Hoverable>
);

export default HoverableView