import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
} from 'react-native';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';



const ListItem = ({ item }:any) => {
  return (
    <View style={styles.item}>
      <Image
        source={{
          uri: item.uri,
        }}
        style={styles.itemPhoto}
      />
        <View>
          <Text style={styles.itemText}>{item.text}</Text>
          <Text style={styles.greetings}>Applied as {item.positon}</Text>
          <Text style={styles.itemText}>{item.hours}</Text>
        </View>

    
      {/* <Text style={styles.itemText}>{item.text}</Text> */}
    </View>
  );
};

export default () => {
  return (
    <View style={styles.container}>
        <SectionList

          contentContainerStyle={{ paddingHorizontal: 10 }}
          stickySectionHeadersEnabled={false}
          sections={SECTIONS}
       
          renderItem={({ item, section }) => {
            return <ListItem item={item} />;
            
          }}
        />
    </View>
  );
};
const SECTIONS = [
    {
      data: [
        {
          key: '1',
          text: 'Connie Barrientos Care',
          positon: 'President',
          uri: 'https://picsum.photos/id/1/200',
          hours: '1m',
        },
        {
          key: '2',
          text: 'Simon Care',
          positon: 'Vice',
          uri: 'https://picsum.photos/id/10/200',
          hours: '1m',
        },
  
        {
          key: '3',
          text: 'Lanz Ruiz',
          positon: 'Senior Developer',
          uri: 'https://picsum.photos/id/1002/200',
          hours: '1m',
        },
        {
          key: '4',
          text: 'Joy Garingo',
          positon: 'Marketing',
          uri: 'https://picsum.photos/id/1006/200',
          hours: '1m',
        },
        {
          key: '5',
          text: 'Dru Hiyas',
          positon: 'Developer',
          uri: 'https://picsum.photos/id/1008/200',
          hours: '1m',
        },
      ],
    },
   
   
  ];
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 35,
  },
  title: {
      fontWeight: '700',
      marginLeft: 20,
      fontSize: 15
  },
  sectionHeader: {
    fontWeight: '800',
    fontSize: 18,
    color: '#f4f4f4',
    marginTop: 20,
    marginBottom: 5,
  },
  item: {
    flexDirection: 'row',
    margin: 10,
    width: wp('90%'),
    height: 100,
    borderRadius: 20,
  },
  itemPhoto: {
    width: 70,
    height: 70,
    borderRadius: 10,
    marginRight: 20
  },
  itemText: {
    color: '#000000',
    marginTop: 5,
  },
  greetings: { 
    paddingTop: hp('1%'),
    fontSize: hp('2%'),
    fontWeight: '500'
  }
});