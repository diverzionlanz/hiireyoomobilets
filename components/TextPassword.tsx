import React from 'react'
import { View, StyleSheet, Text, TextInput, Image } from 'react-native'
import { TextInput as Input } from 'react-native-paper'


interface TextInputProps {
    label?: string,
    returnKeyType?: string,
    onChangeText?: any,
    error?: string,
    errorText?: string,
    autoCapitalize?: string,
    autoCompleteType?: string,
    textContentType?: string,
    keyboardType?: string,
    Placeholder?: string,
}


export default function TextPassword({label,
    returnKeyType,
    onChangeText,
    error,
    errorText,
    autoCapitalize,
    autoCompleteType,
    textContentType,
    keyboardType,
    Placeholder}: TextInputProps) {


    const [secure, setSecure] = React.useState(true);
    return (
      <View style={styles.sectionStyle}>
           
      <TextInput
        style={{flex: 1, marginLeft: 2}}
        placeholder={Placeholder}
        underlineColorAndroid="transparent"
        secureTextEntry={secure}
        
      />
      {/* <Image
              source={{
                uri:
                  'https://raw.githubusercontent.com/AboutReact/sampleresource/master/input_username.png',
              }}
              style={styles.imageStyle}
            /> */}
    
    </View>
    )
  }
  
  const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        backgroundColor: "#e5e5e5",
      },
      sectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderColor: '#d6d5d5',
        height: 50,
        borderRadius: 0,
        margin: 1,
        paddingLeft: 10,
      },
      input: {
          backgroundColor: "transparent",
          height: 46,
          marginTop: 6,
          marginBottom: 10,
          width: "100%",
          marginLeft: 17,
          borderWidth: 0,
          borderColor: "#FFFFFF",
          borderBottomWidth : 1.0,
          borderRightWidth : 0
        },
      imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
      },
    });