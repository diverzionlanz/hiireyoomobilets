import React, {useState, useRef} from 'react'
import { View, StyleSheet, Text, TextInput, Image, TouchableOpacity, Modal} from 'react-native'
import { TextInput as Input } from 'react-native-paper'
import {Picker} from '@react-native-picker/picker';
import SettingsIcon from '../assets/svg/settings'
import Search from '../assets/svg/search'
import Application from '../assets/svg/application'
import { RootTabScreenProps } from '../types';
import { useNavigation } from '@react-navigation/native';
import DropDownPicker from 'react-native-dropdown-picker';
import SelectDropdown from 'react-native-select-dropdown'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';



interface TextInputProps {
    label?: string,
    returnKeyType?: string,
    onChangeText?: any,
    error?: string,
    errorText?: string,
    autoCapitalize?: string,
    autoCompleteType?: string,
    textContentType?: string,
    keyboardType?: string,
    Placeholder?: string,
    function?: void,
}


export default function TextPassword({label,
    returnKeyType,
    onChangeText,
    error,
    errorText,
    autoCapitalize,
    autoCompleteType,
    textContentType,
    keyboardType,
    Placeholder}: TextInputProps) {
      const navigation = useNavigation();

      const [open, setOpen] = useState(false);
      const [value, setValue] = useState('Apple');
      const [items, setItems] = useState([
        {label: 'Apple', value: 'apple'},
        {label: 'Banana', value: 'banana'}
      ]);

    const [secure, setSecure] = React.useState(true);
    const countries = ["Egypt", "Canada", "Australia", "Ireland"]

    return (
      <View style={styles.container}>
         <View style={styles.sectionStyle}>

            <TouchableOpacity>
              <Application status='' />
            </TouchableOpacity>
            <SelectDropdown rowStyle={styles.input}
                    data={countries}
                    onSelect={(selectedItem, index) => {
                        console.log(selectedItem, index)
                    }}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        // text represented after item is selected
                        // if data array is an array of objects then return selectedItem.property to render after item is selected
                        return selectedItem
                    }}
                    rowTextForSelection={(item, index) => {
                        // text represented for each item in dropdown
                        // if data array is an array of objects then return item.property to represent item in dropdown
                        return item
                    }}
            />

            {/* <TextInput
            style={{flex: 1, marginLeft: 6}}
            placeholder={'Category'}
            underlineColorAndroid="transparent"
            
            /> */}
        </View>
        
        
      </View>
      
    )
  }
  
  const styles = StyleSheet.create({
      container: {
        flexDirection: 'row',
      },
      settings: {
        marginLeft: 10,
        alignItems: 'center',
        width: wp('30%'),
        height: 50,
        paddingTop: 10,
        marginTop: 20,
        backgroundColor: '#efefef'
      },
      sectionStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: "#efefef",
        borderWidth: 1,
        borderColor: '#d6d5d5',
        height: 50,
        borderRadius: 0,
        width: '99%',
        marginLeft: 2,
        marginTop: 20,
        paddingLeft: 10,
      },
      input: {
     
          borderColor: "#FFFFFF",
          borderBottomWidth : 1.0,
          borderRightWidth : 0
        },
      imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
      },
    });