import React from 'react'
import { ImageBackground, StyleSheet, KeyboardAvoidingView, ViewStyle, TextStyle, StyleProp,  ImageStyle } from 'react-native'
import { View, Image, Text } from 'react-native';
import SVGSlide1 from '../assets/svg/slide1'
import SVGSlide2 from '../assets/svg/slide2'
import SVGSlide3 from '../assets/svg/slide3'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


interface Styles {
    image: ViewStyle;
  
}

interface slide {
    slideNo:number
}


const LogoCenter = ({slideNo}:slide) => {

    // if(slideNo == 1) {
    //     return (
    //         <SVGLogo style={styles.image} width={200} height={hp('10%')} />
    //     );
    // };
    switch(slideNo) {
        case 1:
            return (
                <SVGSlide1 />
            );
          break;
        case 2:
            return (
                <SVGSlide2 />
            );
          break;
          case 3:
            return (
                <SVGSlide3 />
            );
          break;
        default:
            return (
                <SVGSlide3 />
            );
     }

}


const styles = StyleSheet.create<Styles>({
    image: {
      width: 300,
      height: hp('2%'),
      marginTop: hp('1%'),
      marginBottom: hp('3%'),
    },
});

export default LogoCenter;