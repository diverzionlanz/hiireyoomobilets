import React from 'react'
import { View, StyleSheet, Text, TextInput, Image, TouchableOpacity } from 'react-native'
import { TextInput as Input } from 'react-native-paper'
import SettingsIcon from '../assets/svg/settings'
import LocationIcon from '../assets/svg/location'
import Search from '../assets/svg/search'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';



interface TextInputProps {
    label?: string,
    returnKeyType?: string,
    onChangeText?: any,
    error?: string,
    errorText?: string,
    autoCapitalize?: string,
    autoCompleteType?: string,
    textContentType?: string,
    keyboardType?: string,
    Placeholder?: string,
}


export default function TextPassword({label,
    returnKeyType,
    onChangeText,
    error,
    errorText,
    autoCapitalize,
    autoCompleteType,
    textContentType,
    keyboardType,
    Placeholder}: TextInputProps) {


    const [secure, setSecure] = React.useState(true);
    return (
      <View style={styles.container}>
         <View style={styles.sectionStyle}>

            <TouchableOpacity>
              <LocationIcon />
            </TouchableOpacity>
              
            <TextInput
            style={{flex: 1, marginLeft: 2}}
            placeholder={Placeholder}
            underlineColorAndroid="transparent"
            
            />
        </View>
        
      </View>
      
    )
  }
  
  const styles = StyleSheet.create({
      container: {
        flexDirection: 'row',
      },
      settings: {
        marginLeft: 10,
        alignItems: 'center',
        width: wp('16%'),
        height: 50,
        paddingTop: 10,
        marginTop: 20,
        backgroundColor: '#0097a9'
      },
      sectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderColor: '#d6d5d5',
        height: 50,
        borderRadius: 0,
        width: '90%',
        marginLeft: 25,
        marginTop: 20,
        paddingLeft: 10,
      },
      input: {
          backgroundColor: "transparent",
          height: 46,
          marginTop: 6,
          marginBottom: 10,
          width: "100%",
          marginLeft: 17,
          borderWidth: 0,
          borderColor: "#FFFFFF",
          borderBottomWidth : 1.0,
          borderRightWidth : 0
        },
      imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center',
      },
    });