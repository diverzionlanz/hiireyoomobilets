import React from 'react'
import { View, Image, Text, ImageBackground, StyleSheet, KeyboardAvoidingView, ViewStyle, TextStyle, StyleProp,  ImageStyle } from 'react-native'
import SVGLogo from '../assets/svg/logo'
import SVGSlide from '../assets/svg/slide1'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


interface Styles {
    image: ViewStyle;
  
}

interface AppIconProps {
    height: number;
    width: number;
    image: any;

}

const AppIcon = ({height, width, image}:AppIconProps) => {
    return (
    //    <SVGLogo style={styles.image} width={width} height={height}/>
        <ImageBackground source={image} imageStyle={{ borderRadius: 20, width: width, height: height }} style={styles.image} />
    );
}


const styles = StyleSheet.create<Styles>({
    image: {
    //   width: 30,
      borderRadius: 20,
    //   height: hp('3.5%'),
      marginBottom: hp('3%'),
      marginRight: wp('14%'),
      marginTop: hp('-1%')
    },
});

export default AppIcon;