import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

interface Props {
    label: string,
    press: boolean
}

const RoundedButton = ({ label, press }:Props) => {
  
    return (
    <TouchableOpacity
      style={{ alignItems: 'center', justifyContent: 'center' }}
    
    >
      <Text style={{ fontSize: 22, color: 'white', fontWeight: 'bold' }}>
        {label} 
      </Text>
    </TouchableOpacity>
  );
};

export default RoundedButton;