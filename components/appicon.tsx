import React from 'react'
import { View, Image, Text, ImageBackground, StyleSheet, KeyboardAvoidingView, ViewStyle, TextStyle, StyleProp,  ImageStyle } from 'react-native'
import SVGLogo from '../assets/svg/logo'
import SVGSlide from '../assets/svg/slide1'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


interface Styles {
    image: ViewStyle;
  
}

interface AppIconProps {
    height: number;
    width: number;
    image: any;

}

const AppIcon = ({height, width, image}:AppIconProps) => {
    return (
    //    <SVGLogo style={styles.image} width={width} height={height}/>
        <ImageBackground source={image} style={styles.image} />
    );
}


const styles = StyleSheet.create<Styles>({
    image: {
      width: 50,
      borderRadius: 200,
      height: hp('6%'),
      marginTop: hp('1%'),
      marginBottom: hp('3%'),
      marginLeft: wp('4%'),
    },
});

export default AppIcon;