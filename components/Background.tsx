import React from 'react'
import { ImageBackground, StyleSheet, KeyboardAvoidingView } from 'react-native'
import { View, Image, Text } from 'react-native';


interface Props {
  children: JSX.Element|JSX.Element[];
}

const Background = ({children}:Props) => {
    return (
        <View
          style={styles.background}
        >
          <KeyboardAvoidingView style={styles.container} behavior="padding">
            {children}
          </KeyboardAvoidingView>
        </View>
      )
}


export default Background;

const styles = StyleSheet.create({
    background: {
      flex: 1,
      width: '100%',
      backgroundColor: '#ffffff',
    },
    container: {
      flex: 1,
      padding: 1,
      width: '100%',
      maxWidth: 340,
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
    },
  })
  