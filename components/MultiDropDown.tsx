import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Ionicons, FontAwesome, Feather, AntDesign } from '@expo/vector-icons';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';


const ListItem = ({ item }:any) => {
 
    const renderSwitch = (position: string) => {
        switch(position) {
            case 'UI/UX Designer':
               return '#99d7f9';
            case 'Graphic Artist':
               return '#64d1c4'
               case 'Full Stack Developer':
                return '#0097a7'
            default:
              return '#b1afaf';
        }
    }
  return (
    <View style={styles.item}>
       
        <View style={{width:wp('54%')}}>
          <Text style={{width:wp('20%'), paddingLeft: 7, paddingTop: 3, paddingBottom: 3, backgroundColor: renderSwitch(item.positon)}}>{item.positon}</Text>
        </View>
       

    
      {/* <Text style={styles.itemText}>{item.text}</Text> */}
    </View>
  );
};

export default () => {
  return (
    <View style={styles.container}>
        <View style={{width:'100%', flexDirection: 'row', marginBottom: 10}}>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Full Time</Text></TouchableOpacity>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Part Time</Text></TouchableOpacity>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Contract</Text></TouchableOpacity>
        </View>
        <View style={{width:'100%', flexDirection: 'row'}}>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Full Time1</Text></TouchableOpacity>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Part Time</Text></TouchableOpacity>
            <TouchableOpacity style={styles.greenbutton}><Text style={styles.text}>Contract</Text></TouchableOpacity>
        </View>
    </View>
  );
};
const SECTIONS = [
    {
      data: [
        {
          key: '1',
          company: 'Hireyoo',
          positon: 'UI/UX Designer',
          location: 'Cebu City, Cebu',
          uri: 'https://picsum.photos/id/1/200',
          time: '2 hours'
        },
        {
          key: '2',
          company: 'Hireyoo',
          positon: 'Full Stack Developer',
          uri: 'https://picsum.photos/id/10/200',
          location: 'Cebu City, Cebu',
          time: '2 hours'
        },
      ],
    },
   
   
  ];
const styles = StyleSheet.create({
  container: {
    marginTop: 35,
  },
  title: {
    fontWeight: '700',
    marginLeft: 20,
    fontSize: 15,
    width: wp('70%')
  },
  sectionHeader: {
    fontWeight: '800',
    fontSize: 18,
    color: '#f4f4f4',
    marginTop: 20,
    marginBottom: 5,
    flexDirection: 'row'
  },
  greenbutton: {
    borderRadius: 15,
    backgroundColor: '#0097a9',
    paddingLeft: wp('5%'),
    paddingRight: wp('5%'),
    alignItems: 'center', 
    paddingBottom: hp('1%'),
    paddingTop: hp('1%'),
    marginRight: wp('5%'),
  },
  text: { color: '#FFFFFF' },
  item: {
    margin: 10,
    width: wp('20%'),
    height: 50,
    borderColor: '#eceaea',
    borderWidth: 1,
    borderRadius: 20,
    paddingTop: 20,
    paddingLeft: 15,
  },
  itemPhoto: {
    width: 50,
    height: 50,
    borderRadius: 10,
    marginRight: 20
  },
  itemText: {
    color: '#000000',
    marginTop: 5,
  },
  greetings: { 
    paddingTop: hp('1%'),
    fontSize: hp('2%'),
    fontWeight: '500'
  }
});