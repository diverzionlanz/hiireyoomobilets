import React from 'react'
import { ImageBackground, TouchableOpacity, GestureResponderEvent, StyleSheet, KeyboardAvoidingView, Button, ViewStyle, TextStyle, StyleProp,  ImageStyle, Text } from 'react-native'
import { Button as PaperButton } from 'react-native-paper'
import SVGLogo from '../assets/svg/logo'
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';
import { RootStackScreenProps } from '../types';


interface ButtonProps {
    label: string,
    mode?: string,
    style?: any,
    ClickHandler?: (event: React.MouseEvent<HTMLButtonElement>) => void,
}


const GreenButton = ({ label, ClickHandler }:ButtonProps) => {

  
  return (
  <TouchableOpacity
    style={{ alignItems: 'center', justifyContent: 'center' }}
  
  >
    <Text style={{ fontSize: 22, color: '#000000', fontWeight: 'bold' }}>
      {label}
    </Text>
  </TouchableOpacity>
);
};


const styles = StyleSheet.create({
    button: {
      width: '100%',
      marginVertical: 10,
      paddingVertical: 2,
      borderRadius: 20
    },
    text: {
      fontWeight: 'bold',
      color: '#FFFFFF',
      fontSize: 15,
      lineHeight: 26,
    },
  })
  

export default GreenButton;

// source: https://www.pluralsight.com/guides/typescript-pass-function-react