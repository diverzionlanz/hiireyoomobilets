import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  SectionList,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Ionicons, FontAwesome, Feather, AntDesign } from '@expo/vector-icons';
import AppIcon from '../components/appicon'
const image = { uri: "https://reactjs.org/logo-og.png" };
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';



const ListItem = ({ item }:any) => {
  const [bg, setbg] = useState('#f2f3f3');
  const [positionBg, setPositionBg] = useState('#ffa443');
  const [textColor, setTextColor] = useState('#000000');
  const [iconColor, setIconColor] = useState('#000000');
  const [heartColor, setHeartColor] = useState('#ffffff');
  const [timeColor, setTimeColor] = useState('#000000');
  return (
    <View style={styles.item}>
      <Image
        source={{
          uri: item.uri,
        }}
        style={styles.itemPhoto}
        resizeMode="cover"
      />
      <View style={{position: "absolute", top: 1, flexDirection: 'row', height: hp('80%')}}>
          <View style={{width: '83%'}}>
              <AppIcon image={item.profile} height={2} width={2} />
          </View>
          <View style={{width: '20%', marginTop: 10}}>
              <TouchableOpacity onPress={()=>{setbg('#013954'), setPositionBg('#0097a7'), setTextColor('#ffffff'), setIconColor('#ffffff'), setHeartColor('#0097a7'), setTimeColor('#ffffff')}}>
                  <AntDesign name="heart" size={24} color={heartColor} />
              </TouchableOpacity>
          </View>
      </View>
      <View style={{position: "absolute", flexDirection: 'row', top: 70, paddingBottom: hp('3%'), width: wp('90%'), borderBottomRightRadius: 20, borderBottomLeftRadius: 20, height: hp('11%'), backgroundColor: bg, paddingLeft: 10, paddingRight: 10, paddingTop: 10}}>
          <View style={{width: wp('67%')}}>
              <Text style={{color: textColor, marginBottom: 6}}>{item.name}</Text>
              <View style={{backgroundColor: positionBg, padding: 5, width:wp('40%')}}><Text style={{color: '#ffffff', fontSize: hp('1.5%')}}>{item.position}</Text></View>
              <Text style={{color: textColor, fontSize: 10}}>58k - Cebu City, Philippines</Text>
                    
          </View>
          <View style={{width: '18%', alignItems: 'center'}}>
              <AntDesign name="ellipsis1" size={30} style={{marginTop:-6, marginLeft: wp('3%')}} color={iconColor} />
              <Text style={{fontSize: 10, marginTop: hp('2%'), width: wp('25%'), marginLeft: -50, textAlign: 'right',fontWeight: '500', color: timeColor}}>{item.status}</Text>
          </View>
                 
      </View>
    
      {/* <Text style={styles.itemText}>{item.text}</Text> */}
    </View>
  );
};

export default () => {
  return (
    <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
           <Text style={styles.title}>Featured applicants</Text>
           <TouchableOpacity><Text style={{textAlign:'right'}}>View More</Text></TouchableOpacity>
        </View>
        <SectionList

          contentContainerStyle={{ paddingHorizontal: 10 }}
          stickySectionHeadersEnabled={false}
          sections={SECTIONS}
       
          renderItem={({ item, section }) => {
            return <ListItem item={item} />;
            
          }}
        />
    </View>
  );
};
const SECTIONS = [
    {
      title: 'Made for you',
      data: [
        {
          key: '1',
          text: 'Item text 1',
          name: 'Connie Barrientos Carey',
          uri: 'https://picsum.photos/id/1/200',
          profile: {uri: 'https://assets-global.website-files.com/5ec7dad2e6f6295a9e2a23dd/5edfa7c6f978e75372dc332e_profilephoto1.jpeg'},
          position: 'Recruitement Specialiist',
          status: 'Open to work'
        },
        {
          key: '2',
          text: 'Item text 2',
          name: 'Lanz Ruiz',
          uri: 'https://picsum.photos/id/10/200',
          profile: {uri: 'https://assets-global.website-files.com/5ec7dad2e6f6295a9e2a23dd/5edfa7c6f978e75372dc332e_profilephoto1.jpeg'},
          position: 'Full Stack Developer',
          status: 'Hired'
        },
  
        {
          key: '3',
          text: 'Item text 3',
          name: 'John Mchome',
          uri: 'https://picsum.photos/id/1002/200',
          profile: {uri: 'https://assets-global.website-files.com/5ec7dad2e6f6295a9e2a23dd/5edfa7c6f978e75372dc332e_profilephoto1.jpeg'},
          position: 'Graphic Designer',
          status: 'Freelancer'
        },
      
      ],
    },
  ];
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 35,
  },
  title: {
    fontWeight: '700',
    marginLeft: 20,
    fontSize: 15,
    width: wp('70%')
  },
  sectionHeader: {
    fontWeight: '800',
    fontSize: 18,
    color: '#f4f4f4',
    marginTop: 20,
    marginBottom: 5,
  },
  item: {
    margin: 10,
    marginBottom: 30,
    width: wp('90%'),
    height: 150,
    backgroundColor: '#000000',
    borderRadius: 20,
  },
  itemPhoto: {
    width: '100%',
    height: hp('18%'),
    borderRadius: 20,
  },
  itemText: {
    color: '#000000',
    marginTop: 5,
  },
});