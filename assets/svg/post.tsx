import * as React from "react";
import Svg, { Path, G } from "react-native-svg";

interface Props {
    status: string;
    width?: number;
    height?: number;
}

const Selected = () => {
    return ( 
        <G
        id="Group_224"
        data-name="Group 224"
        transform="translate(-195.222 -765.994)"
      >
        <Path
          id="Path_290"
          data-name="Path 290"
          d="M210.222,914.759v20"
          transform="translate(-714.537 986.216) rotate(-90)"
          fill="none"
          stroke="#0097a9"
          strokeWidth={1}
        />
        <Path
          id="Path_291"
          data-name="Path 291"
          d="M210.222,914.759v20"
          transform="translate(0 -148.764)"
          fill="none"
          stroke="#0097a9"
          strokeWidth={1}
        />
      </G>
    );
}

const NotSelected = () => {
    return (
        <G
      id="Group_224"
      data-name="Group 224"
      transform="translate(-195.222 -765.994)"
    >
      <Path
        id="Path_290"
        data-name="Path 290"
        d="M210.222,914.759v20"
        transform="translate(-714.537 986.216) rotate(-90)"
        fill="none"
        stroke="#707070"
        strokeWidth={1}
      />
      <Path
        id="Path_291"
        data-name="Path 291"
        d="M210.222,914.759v20"
        transform="translate(0 -148.764)"
        fill="none"
        stroke="#707070"
        strokeWidth={1}
      />
    </G>
    );
}

const PostSvg = ({status}:Props) => (
  <Svg
    width={31}
    height={26.347}
    viewBox="0 0 31 26.347"
  >
   {status === 'selected' ? <Selected /> : <NotSelected />}
    
  </Svg>
);

export default PostSvg;
