import * as React from "react";
import Svg, { Path } from "react-native-svg";

interface Props {
    status: string;
    width?: number;
    height?: number;
}

const Selected = () => {
    return ( 
        <Path
        id="Path_170"
        data-name="Path 170"
        d="M-616.039-2965.963l-15,11.875v13.334h11.979v-5.66a3.02,3.02,0,0,1,3.021-3.021,3.021,3.021,0,0,1,3.021,3.021v5.66h11.979v-13.334Z"
        transform="translate(631.539 2966.601)"
        fill="none"
        stroke="#0097a9"
        strokeWidth={1}
      />
    );
}

const NotSelected = () => {
    return (
        <Path
        id="Path_170"
        data-name="Path 170"
        d="M-616.039-2965.963l-15,11.875v13.334h11.979v-5.66a3.02,3.02,0,0,1,3.021-3.021,3.021,3.021,0,0,1,3.021,3.021v5.66h11.979v-13.334Z"
        transform="translate(631.539 2966.601)"
        fill="none"
        stroke="#707070"
        strokeWidth={1}
      />
    );
}

const HomeSvg = ({status}:Props) => (
  <Svg
    width={31}
    height={26.347}
    viewBox="0 0 31 26.347"
  >
   {status === 'selected' ? <Selected /> : <NotSelected />}
    
  </Svg>
);

export default HomeSvg;
