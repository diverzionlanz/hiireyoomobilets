import * as React from "react";
import Svg, { Path, Circle, G } from "react-native-svg";

interface Props {
    status: string;
    width?: number;
    height?: number;
}

const Selected = () => {
    return ( 
        <G id="Group_231" data-name="Group 231" transform="translate(-578 -3944)">
      <G
        id="Ellipse_27"
        data-name="Ellipse 27"
        transform="translate(578 3944)"
        fill="none"
        stroke="#0097a9"
        strokeWidth={1}
      >
        <Circle cx={9} cy={9} r={9} stroke="none" />
        <Circle cx={9} cy={9} r={8.5} fill="none" />
      </G>
      <Path
        id="Path_295"
        data-name="Path 295"
        d="M591.048,3888.908l6.4,6.4"
        transform="translate(2 70)"
        fill="#fff"
        stroke="#0097a9"
        strokeWidth={1}
      />
    </G>
    );
}

const NotSelected = () => {
    return (
        <G id="Group_231" data-name="Group 231" transform="translate(-578 -3944)">
         <G
        id="Ellipse_27"
        data-name="Ellipse 27"
        transform="translate(578 3944)"
        fill="none"
        stroke="#7b7b7b"
        strokeWidth={1}
      >
        <Circle cx={9} cy={9} r={9} stroke="none" />
        <Circle cx={9} cy={9} r={8.5} fill="none" />
      </G>
      <Path
        id="Path_295"
        data-name="Path 295"
        d="M591.048,3888.908l6.4,6.4"
        transform="translate(2 70)"
        fill="#fff"
        stroke="#7b7b7b"
        strokeWidth={1}
      />
    </G>
    );
}

const SearchSvg = ({status}:Props) => (
  <Svg
    width={31}
    height={26.347}
    viewBox="0 0 31 26.347"
  >
   {status === 'selected' ? <Selected /> : <NotSelected />}
    
  </Svg>
);

export default SearchSvg;
