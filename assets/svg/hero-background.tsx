import * as React from "react";
import {Text} from "react-native"
import Svg, { Defs, LinearGradient, Stop, G, Path } from "react-native-svg";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
  } from 'react-native-responsive-screen';

import Logo from "../svg/logo"



const SVGComponent = () => (
  <Svg

    width={wp('100%')}
    height={hp('100%')}
    viewBox="20 313 1700.407 796.068"
  >
    <Defs>
      <LinearGradient
        id="linear-gradient"
        x1={0.5}
        x2={0.5}
        y2={1}
        gradientUnits="objectBoundingBox"
      >
        <Stop offset={0} stopColor="#0097a9" />
        <Stop offset={1} stopColor="#00a1b5" />
      </LinearGradient>
    </Defs>
    {/* <Logo width={140} height={hp('10%')} Iconcolor='#ffffff' Textcolor='#ffffff' /> */}
    <G id="hero-bg" transform="translate(0.004)"> 
      <Path
        id="Path_1"
        data-name="Path 1"
        d="M1650.59,755.5c56.3,272.618-131.189,364.024-310.13,505.374-347.561,273.959-503.57,14.469-1065.8,253.673S-85.172,755.5-85.172,755.5Z"
        transform="translate(120.633 -755.5)"
        fill="#0097a9"
      />
      <Path
        id="Path_2"
        data-name="Path 2"
        d="M-113.148,755.5c-41.4,185.71,96.459,247.977,228.027,344.266,255.548,186.624,370.256,9.856,783.643,172.8s264.57-517.07,264.57-517.07Z"
        transform="translate(286.464 -753.856)"
        fill="url(#linear-gradient)"
      />
    </G>
  </Svg>
);

export default SVGComponent;
