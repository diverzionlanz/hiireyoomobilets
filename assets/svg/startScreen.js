import React from "react"
import Svg, { G, Path, Ellipse } from "react-native-svg"

function SvgComponent(props) {
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={331.707}
      height={194.013}
      viewBox="0 0 331.707 194.013"
      {...props}
    >
      <G data-name="Group 18" transform="translate(-21.646 -201.274)">
        <Path
          data-name="Path 9"
          d="M288.349 314.616a1.645 1.645 0 00-2.045 1.109c-2.263 7.619-3.24 11.549-3.6 14.5a26.669 26.669 0 00.4 8.342c.17 1.1.364 2.353.557 3.855.564 4.39.12 7.219-1.4 8.9-1.7 1.89-5.066 2.708-10.58 2.57a58.042 58.042 0 01-11.047-1.365c-3.626-.737-6.246-1.27-9.377.226a1.645 1.645 0 101.419 2.969 7.1 7.1 0 014.321-.513l-9.037 39.826h1.32l10.451-39.335.25.051a60.949 60.949 0 0011.621 1.43q.627.015 1.223.015a32.86 32.86 0 004.221-.249l9.129 38.087h1.319l-7.711-38.634a10.1 10.1 0 004.924-2.87c2.209-2.45 2.914-6.113 2.218-11.527-.2-1.544-.395-2.815-.569-3.938-.9-5.82-1.114-7.2 3.1-21.4a1.646 1.646 0 00-1.107-2.049z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 10"
          d="M214.612 245.053a40.546 40.546 0 00.253-4.5c0-.8-.027-1.6-.073-2.391l-7.437-2.406a32.67 32.67 0 00-2.633-8.91l4.931-6.068a40.137 40.137 0 00-3.906-5.678l-7.437 2.427a33.042 33.042 0 00-7.363-5.659l.423-7.814a39.808 39.808 0 00-6.492-2.3l-4.593 6.337a33.106 33.106 0 00-9.284-.243l-4.252-6.572a39.721 39.721 0 00-6.606 1.952l.01 7.826a32.947 32.947 0 00-7.652 5.263l-7.3-2.815a40.287 40.287 0 00-4.2 5.463l4.6 6.321a32.682 32.682 0 00-3.1 8.758l-7.552 2.01a40.514 40.514 0 00-.253 4.5c0 .8.026 1.6.073 2.391l7.436 2.406a32.672 32.672 0 002.634 8.91l-4.931 6.068a40.135 40.135 0 003.906 5.678l7.437-2.427a33.047 33.047 0 007.363 5.66l-.423 7.814a39.835 39.835 0 006.492 2.3l4.593-6.338a33 33 0 009.284.243l4.252 6.572a39.748 39.748 0 006.606-1.952l-.01-7.825a32.994 32.994 0 007.652-5.264l7.3 2.815a40.285 40.285 0 004.2-5.463l-4.6-6.321a32.68 32.68 0 003.1-8.758zm-39.831 11.221a15.722 15.722 0 1115.719-15.722 15.722 15.722 0 01-15.719 15.722z"
          fill="#0097a9"
        />
        <G data-name="Group 4">
          <Path
            data-name="Path 11"
            d="M196.224 258.483a.588.588 0 01-.452-.964 26.6 26.6 0 004.687-25.618.588.588 0 011.111-.383 27.774 27.774 0 01-4.894 26.753.586.586 0 01-.452.212z"
            fill="#fff"
          />
        </G>
        <G data-name="Group 5">
          <Path
            data-name="Path 12"
            d="M193.948 221.376a.584.584 0 01-.4-.16 26.392 26.392 0 00-9.141-5.632.588.588 0 01.4-1.1 27.573 27.573 0 019.547 5.881.588.588 0 01-.4 1.016z"
            fill="#fff"
          />
        </G>
        <G data-name="Group 6">
          <Path
            data-name="Path 13"
            d="M168.683 214.823a.587.587 0 01-.144-1.157 27.828 27.828 0 016.808-.842.587.587 0 010 1.175 26.658 26.658 0 00-6.52.806.6.6 0 01-.144.018z"
            fill="#fff"
          />
        </G>
        <Path
          data-name="Path 14"
          d="M88.252 344.6a28.135 28.135 0 00.175-3.117c0-.556-.018-1.108-.051-1.656l-5.151-1.666a22.638 22.638 0 00-1.823-6.172l3.415-4.2a27.948 27.948 0 00-2.705-3.933l-5.152 1.681a22.894 22.894 0 00-5.1-3.92l.293-5.412a27.571 27.571 0 00-4.5-1.594L64.475 319a22.868 22.868 0 00-6.43-.168l-2.945-4.556a27.576 27.576 0 00-4.576 1.352l.007 5.421a22.779 22.779 0 00-5.3 3.646l-5.054-1.95a27.891 27.891 0 00-2.911 3.784l3.19 4.378a22.641 22.641 0 00-2.15 6.066l-5.232 1.392a28.134 28.134 0 00-.175 3.117c0 .556.019 1.108.051 1.656l5.15 1.662a22.632 22.632 0 001.823 6.171l-3.415 4.2a27.813 27.813 0 002.706 3.933l5.151-1.681a22.865 22.865 0 005.1 3.92l-.293 5.412a27.477 27.477 0 004.5 1.594l3.181-4.39a22.928 22.928 0 006.43.169l2.945 4.552a27.576 27.576 0 004.576-1.352l-.007-5.421a22.806 22.806 0 005.3-3.645l5.054 1.95a27.953 27.953 0 002.911-3.784l-3.19-4.379a22.583 22.583 0 002.15-6.066zm-27.589 7.773a10.89 10.89 0 1110.89-10.89 10.89 10.89 0 01-10.89 10.889z"
          fill="#0097a9"
        />
        <G data-name="Group 7">
          <Path
            data-name="Path 15"
            d="M75.515 354.082a.587.587 0 01-.451-.963 18.244 18.244 0 003.214-17.57.588.588 0 011.111-.383 19.419 19.419 0 01-3.422 18.7.584.584 0 01-.452.216z"
            fill="#fff"
          />
        </G>
        <G data-name="Group 8">
          <Path
            data-name="Path 16"
            d="M73.939 328.38a.583.583 0 01-.4-.159 18.1 18.1 0 00-6.269-3.863.588.588 0 01.4-1.105 19.269 19.269 0 016.675 4.112.587.587 0 01-.4 1.015z"
            fill="#fff"
          />
        </G>
        <G data-name="Group 9">
          <Path
            data-name="Path 17"
            d="M56.439 323.842a.588.588 0 01-.143-1.158 19.483 19.483 0 014.759-.588.587.587 0 010 1.175 18.284 18.284 0 00-4.472.553.592.592 0 01-.144.018z"
            fill="#fff"
          />
        </G>
        <Path
          data-name="Path 18"
          d="M238.616 274.994c0-.3-.009-.606-.025-.906l3.859-3.1a20.525 20.525 0 00-1.4-4.434l-4.983-.377a16.591 16.591 0 00-4.314-4.577l-.016-4.965a20.573 20.573 0 00-4.419-1.7l-3.4 3.666a16.566 16.566 0 00-6.124.48l-3.9-3.093a20.686 20.686 0 00-4.131 2.4l.739 4.923a16.523 16.523 0 00-3.479 5.077l-4.858 1.126a20.656 20.656 0 00-.718 4.683l4.279 2.474a16.394 16.394 0 001.83 6.047L205.4 287.2a20.8 20.8 0 003.148 3.426l4.589-1.8a16.386 16.386 0 006.049 2.4l2.163 4.453c.258.009.516.016.777.016a20.8 20.8 0 003.794-.348l1.458-4.716a16.475 16.475 0 005.694-3.306l4.822 1.084a20.705 20.705 0 002.569-3.794l-2.795-4.106a16.462 16.462 0 00.948-5.515zm-16.673 7.266a6.947 6.947 0 116.946-6.946 6.946 6.946 0 01-6.946 6.946z"
          fill="#0097a9"
        />
        <Path
          data-name="Rectangle 4"
          fill="#0097a9"
          d="M21.646 237.828h51.093v83.744H21.646z"
        />
        <Path
          data-name="Rectangle 5"
          fill="#fff"
          d="M24.322 239.534h46.737v78.846H24.322z"
        />
        <Path
          data-name="Path 19"
          d="M38.3 271.37c1.759-3.785 4.736-5 9.625-5 5.822 0 8.844 2.109 9.7 5.18-10.425.15-19.325-.18-19.325-.18z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 20"
          d="M45.894 247.749c-2.464.692-2.356 2.076-3.177 3.362s-2.606 2.142-2.821 3 .55 2.642-.357 3.641-1.213 1.5-.856 2.5 1.142 2.571 1.213 2.785a4 4 0 002.285 1.928 25.7 25.7 0 014.641 1.071c1.143.5 4.816-.64 6.855-.568s3.428-1.431 3.285-2.431 1.5-3 1.5-4.713c0-1.84-2.142-3.213-2.713-4.427s.071-3.355-2.071-4.624a12.318 12.318 0 00-3.356-1.088 7.945 7.945 0 00-4.428-.436z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 21"
          d="M48.893 269.176c-.557.822-.966.857-1.571 0a7.826 7.826 0 01-1.428-3.015v-7.839a1.433 1.433 0 011.428-1.428h1.571a1.432 1.432 0 011.428 1.428v7.839a7.93 7.93 0 01-1.428 3.015z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 22"
          d="M54.089 255.717a1.227 1.227 0 00-1.3.314c-.337-3.123-2.293-5.527-4.678-5.527-2.435 0-4.419 2.507-4.694 5.724a1.341 1.341 0 00-1.5-.511c-.624.3-.769 1.294-.325 2.229s1.31 1.453 1.933 1.157c.042-.02.064-.066.1-.092.629 2.569 2.388 4.433 4.485 4.433 2.065 0 3.8-1.807 4.458-4.315.615.216 1.423-.289 1.847-1.183.442-.935.297-1.933-.326-2.229z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 23"
          d="M49.893 260.483a2.7 2.7 0 01-1.785 1.036 2.7 2.7 0 01-1.785-1.036z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 24"
          d="M53.76 281.415H41.769a1.395 1.395 0 010-2.789H53.76a1.395 1.395 0 010 2.789z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 25"
          d="M55.956 285.7H39.573a1.395 1.395 0 010-2.789h16.383a1.395 1.395 0 010 2.789z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 26"
          d="M67.689 301.106H27.84a.809.809 0 010-1.619h39.849a.809.809 0 010 1.619z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 27"
          d="M67.689 304.866H27.84a.809.809 0 010-1.618h39.849a.809.809 0 010 1.618z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 28"
          d="M67.689 308.626H27.84a.809.809 0 010-1.618h39.849a.809.809 0 010 1.618z"
          fill="#0097a9"
        />
        <G data-name="Group 10">
          <Path
            data-name="Path 29"
            d="M62.116 272.081h-28.7a1.446 1.446 0 01-1.444-1.444v-26.454a1.446 1.446 0 011.444-1.444h28.7a1.446 1.446 0 011.444 1.444v26.454a1.446 1.446 0 01-1.444 1.444zm-28.7-28.167a.273.273 0 00-.269.269v26.454a.272.272 0 00.269.269h28.7a.272.272 0 00.269-.269v-26.454a.273.273 0 00-.269-.269z"
            fill="#0097a9"
          />
        </G>
        <G data-name="Group 11" fill="#0097a9">
          <Path
            data-name="Path 30"
            d="M37.555 289.336l.831 1.684 1.858.27-1.344 1.31.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 31"
            d="M44.361 289.336l.831 1.684 1.858.27-1.344 1.31.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 32"
            d="M51.168 289.336L52 291.02l1.858.27-1.346 1.31.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 33"
            d="M57.974 289.336l.831 1.684 1.858.27-1.345 1.31.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
        </G>
        <Path
          data-name="Rectangle 6"
          fill="#0097a9"
          d="M135.552 237.828h51.093v83.744h-51.093z"
        />
        <Path
          data-name="Rectangle 7"
          fill="#fff"
          d="M138.228 239.534h46.737v78.846h-46.737z"
        />
        <Path
          data-name="Path 34"
          d="M154.427 256.547c0 3.751 2.258 9.8 7.586 9.609s6.506-5.964 6.712-7.592-14.298-2.017-14.298-2.017z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 35"
          d="M152.207 271.5c1.759-3.785 4.736-5 9.626-5 5.821 0 8.844 2.109 9.7 5.179-10.424.144-19.326-.179-19.326-.179z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 36"
          d="M162.8 269.176c-.557.822-.965.857-1.57 0a7.826 7.826 0 01-1.428-3.015v-7.839a1.432 1.432 0 011.428-1.428h1.57a1.432 1.432 0 011.428 1.428v7.839a7.93 7.93 0 01-1.428 3.015z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Ellipse
          data-name="Ellipse 1"
          cx={4.748}
          cy={6.47}
          rx={4.748}
          ry={6.47}
          transform="translate(157.265 250.504)"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 37"
          d="M164.623 252.056c-.564 1.99-1.8 1.967-3.525 2.62a4.672 4.672 0 00-3.112 2.584c-.72 1.623.326 6.355.326 6.355s-4.365-3.267-3.86-8.4 3.534-7.128 6.385-7.128a4.57 4.57 0 013.935 1.737 4.273 4.273 0 012.955 1.708c1.93 2.287 1.336 7.84 0 9.83s-2.228 2.614-2.2 2.524 1.247-4.658.594-7.483a18.027 18.027 0 00-1.498-4.347z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 38"
          d="M163.8 260.483a2.056 2.056 0 01-3.57 0z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 39"
          d="M167.665 281.415h-11.991a1.395 1.395 0 010-2.789h11.991a1.395 1.395 0 110 2.789z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 40"
          d="M169.861 285.7h-16.382a1.395 1.395 0 110-2.789h16.382a1.395 1.395 0 010 2.789z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 41"
          d="M181.6 301.106h-39.85a.809.809 0 010-1.619h39.85a.809.809 0 010 1.619z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 42"
          d="M181.6 304.866h-39.85a.809.809 0 010-1.618h39.85a.809.809 0 110 1.618z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 43"
          d="M181.6 308.626h-39.85a.809.809 0 010-1.618h39.85a.809.809 0 110 1.618z"
          fill="#0097a9"
        />
        <G data-name="Group 12">
          <Path
            data-name="Path 44"
            d="M176.021 272.081h-28.7a1.446 1.446 0 01-1.444-1.444v-26.454a1.446 1.446 0 011.444-1.444h28.7a1.446 1.446 0 011.445 1.444v26.454a1.446 1.446 0 01-1.445 1.444zm-28.7-28.167a.274.274 0 00-.269.269v26.454a.273.273 0 00.269.269h28.7a.272.272 0 00.269-.269v-26.454a.273.273 0 00-.269-.269z"
            fill="#0097a9"
          />
        </G>
        <G data-name="Group 13" fill="#0097a9">
          <Path
            data-name="Path 45"
            d="M151.46 289.336l.831 1.684 1.858.27-1.349 1.31.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 46"
            d="M158.267 289.336l.831 1.684 1.858.27-1.345 1.311.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 47"
            d="M165.073 289.336l.831 1.684 1.858.27-1.345 1.311.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
          <Path
            data-name="Path 48"
            d="M171.879 289.336l.831 1.684 1.858.27-1.345 1.311.317 1.851-1.662-.874-1.662.874.317-1.851-1.345-1.311 1.858-.27z"
          />
        </G>
        <G
          data-name="Rectangle 8"
          fill="#0097a9"
          stroke="#fff"
          strokeMiterlimit={10}
        >
          <Path stroke="none" d="M69.133 222.509h69.785V336.89H69.133z" />
          <Path fill="none" d="M68.633 222.009h70.785V337.39H68.633z" />
        </G>
        <Path
          data-name="Rectangle 9"
          fill="#fff"
          d="M72.107 226.854h63.836v107.691H72.107z"
        />
        <Path
          data-name="Path 49"
          d="M98.076 249.474c-.044-.134-1.162-6.549-1.3-9.043s4.01-2.851 4.01-2.851 3.385-1.827 5.835-.891a9.874 9.874 0 015.232 4.469c1.113 1.96-.98 8.069-.98 8.069z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 50"
          d="M90.395 270.327c-.015-.105.287-2.63.3-2.732.565-5.316 7.117-6.811 13.656-6.811 7.683 0 12.895 1.406 13.726 6.93.024.162.468 2.693.446 2.863-14.242.195-28.128-.25-28.128-.25z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 51"
          d="M105.667 264.667c-.761 1.124-1.319 1.171-2.145 0-.657-.93-2.851-3.053-2.851-4.126l.9-10.7a1.957 1.957 0 011.951-1.95h2.145a1.957 1.957 0 011.951 1.95l.418 10.7c0 1.073-1.686 3.118-2.369 4.126z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 52"
          d="M101.165 264.251l2.554 1.714-.792 4.55 3.445-.192-.86-4.138 2.117-1.933-.995-.937h-4.474z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 53"
          d="M100.849 258.2l-3.159 3.007s3.219 5.724 3.4 5.724 2.731-3.156 2.731-3.156l1.635-.028 2.583 2.709 3-5.309-3.06-3.244s-2.614 5.049-3.089 5.168-.478.178-.863-.119-3.178-4.752-3.178-4.752z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 54"
          d="M112.764 246.285a1.665 1.665 0 00-1.764.415c-.464-4.261-3.152-7.533-6.407-7.533-3.323 0-6.057 3.406-6.437 7.793a1.819 1.819 0 00-2.025-.672c-.851.4-1.05 1.767-.443 3.044s1.788 1.985 2.64 1.581a1.174 1.174 0 00.124-.084c.865 3.494 3.285 6.012 6.141 6.012 2.821 0 5.215-2.459 6.107-5.887.838.278 1.929-.409 2.5-1.622.613-1.28.415-2.643-.436-3.047z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 55"
          d="M107.472 251.627s-1.288 2.087-2.877 2.087-2.878-2.087-2.878-2.087z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 56"
          d="M100.79 241.99c2.673-1.871 3.875-1.559 5.88 1.515s4.41 3.127 4.41 3.127l.312-3.187s-2.986-5.776-6.995-5.019a10.58 10.58 0 00-6.288 4.366s-.195 6.763.364 6.994.624-2.183 1.025-2.673-.4-2.807.134-3.876 1.158-1.247 1.158-1.247z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 57"
          d="M109.387 245.465c.934 1.021.58 5.078 1.07 5.033s.668-3.875.534-4.276-1.604-.757-1.604-.757z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 58"
          d="M119.021 284.057h-29.79a1.9 1.9 0 110-3.809h29.79a1.9 1.9 0 010 3.809z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 59"
          d="M115.314 289.908H92.938a1.9 1.9 0 010-3.809h22.376a1.9 1.9 0 010 3.809z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 60"
          d="M131.34 310.951H76.912a1.1 1.1 0 110-2.21h54.428a1.1 1.1 0 110 2.21z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 61"
          d="M131.34 316.087H76.912a1.1 1.1 0 110-2.21h54.428a1.1 1.1 0 110 2.21z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 62"
          d="M131.34 321.223H76.912a1.1 1.1 0 110-2.21h54.428a1.1 1.1 0 110 2.21z"
          fill="#0097a9"
        />
        <G data-name="Group 14">
          <Path
            data-name="Path 63"
            d="M123.727 271.094h-39.2a1.76 1.76 0 01-1.758-1.758V233.2a1.76 1.76 0 011.758-1.757h39.2a1.759 1.759 0 011.758 1.757v36.132a1.76 1.76 0 01-1.758 1.762zm-39.2-38.472a.583.583 0 00-.583.582v36.132a.583.583 0 00.583.582h39.2a.583.583 0 00.583-.582V233.2a.583.583 0 00-.583-.582z"
            fill="#0097a9"
          />
        </G>
        <G data-name="Group 15" fill="#0097a9">
          <Path
            data-name="Path 64"
            d="M85.459 294.876l1.135 2.3 2.538.369-1.836 1.79.434 2.528-2.27-1.193-2.27 1.193.434-2.528-1.836-1.79 2.538-.369z"
          />
          <Path
            data-name="Path 65"
            d="M94.756 294.876l1.135 2.3 2.538.369-1.836 1.79.434 2.528-2.27-1.193-2.27 1.193.434-2.528-1.836-1.79 2.538-.369z"
          />
          <Path
            data-name="Path 66"
            d="M104.052 294.876l1.135 2.3 2.538.369-1.836 1.79.434 2.528-2.27-1.193-2.27 1.193.434-2.528-1.836-1.79 2.538-.369z"
          />
          <Path
            data-name="Path 67"
            d="M113.348 294.876l1.135 2.3 2.538.369-1.836 1.79.434 2.528-2.27-1.193-2.27 1.193.434-2.528-1.836-1.79 2.538-.369z"
          />
          <Path
            data-name="Path 68"
            d="M122.792 294.876l1.135 2.3 2.538.369-1.836 1.79.433 2.528-2.27-1.193-2.27 1.193.433-2.528-1.836-1.79 2.538-.369z"
          />
        </G>
        <Path
          data-name="Path 69"
          d="M255.749 367s8.367 12.994 9.5 13.559 3.955.707 2.895 2.825-1.483 10.452-2.471 10.876-9.464 1.413-8.193 0 2.054-.706 2.7-3.249 1.543-7.344.342-8.333-8.545-9.887-8.545-9.887z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 70"
          d="M213.641 372.579s-10.17 16.762-12.054 18.269-8.1 3.767-3.013 4.143 7.533-1.7 10.735-2.636 7.157-1.507 5.65-3.955-.881-7.794 2.367-11.184-3.685-4.637-3.685-4.637z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 71"
          d="M265.54 333.711s-25.176-.658-35.158 7.533c-5.838 8.852-19.962 34.969-21.06 37.848.753.753 4.991 2.112 6.874 1.924 5.556-6.625 12.9-12.352 15.82-17.548 3.6-6.4 4.709-9.6 10.547-10.17s19.022-2.449 22.977-2.449 0-17.138 0-17.138z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 72"
          d="M279.883 331.986l-14.637 2.527s-19.451.179-28.342 1.835c-8.089 1.506-9.281 10.7 0 23.353 5.272 7.189 9.73 12.924 18.845 19.391 2.319-1.7 3.206-2.347 3.847-6.443-.66-.9-2.368-6.567-2.9-7.675-5.042-10.539-11.772-14.372-11.865-15.409 1.13-1.836 29.931 9.571 35.658 2.861 5.469-6.407-.606-20.44-.606-20.44z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 73"
          d="M277.534 286.916l-3.425 1.012a7.525 7.525 0 00-.057-3.819c-1.213-4.277-4.843-6.994-8.108-6.068s-4.927 5.144-3.714 9.422 3.543 7.184 6.808 6.258a6.875 6.875 0 003.247-2.215c.723 2.562 1.267 6.238-.824 7.981a32.125 32.125 0 00-2.423 2.212 15.318 15.318 0 0111.453-2.85 45.967 45.967 0 01-2.957-11.933z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 74"
          d="M276.122 300.617c-6.4 2.238-5.691 8.982-11.582 18.927-3.049 5.146-5.881 5.07-5.881 5.07a136.367 136.367 0 01-15.8-1c-5.415-.662-5.759.95-1.993 2.3 7.6 2.716 15.479 5.639 21.835 4.933s17.229-16.927 17.79-21.147c.775-5.84-1.146-10.211-4.369-9.083z"
          fill="#f09959"
        />
        <Path
          data-name="Path 75"
          d="M214.332 386.707c-6.748 3.3-10.38 5.191-12.543 5.011-.511-.042-.934-.1-1.289-.15-2.407 1.422-6.248 3.1-1.926 3.423 5.085.377 7.533-1.7 10.735-2.636s7.157-1.507 5.65-3.955a5.188 5.188 0 01-.627-1.693z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 76"
          d="M265.981 380.834c-.165 5.343-.9 10.637-4.167 11.637a5.491 5.491 0 01-2.354.21c-.514.636-1.141.643-1.983 1.578-1.271 1.413 7.2.424 8.193 0s1.412-8.757 2.471-10.876c.871-1.74-.88-2.146-2.16-2.549z"
          fill="#0097a9"
        />
        <G data-name="Group 16">
          <Path
            data-name="Path 77"
            d="M239.988 330.271h-21.437a1.645 1.645 0 01-1.493-.952l-8.658-18.645a1.645 1.645 0 012.984-1.387l8.221 17.694h20.388a1.645 1.645 0 110 3.29z"
            fill="#0097a9"
          />
        </G>
        <Path
          data-name="Path 78"
          d="M244.474 323.8c-4.332-.063-8.628-2.645-10.763-2.268s-9.1 5.211-8.223 5.4 6.4-2.675 6.591-2.719a9.29 9.29 0 004.332 2.728c2.95.744.021-2.211 9.167.551 5.947 1.795-1.104-3.692-1.104-3.692z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 79"
          d="M265.19 306.856s-2.9 7.121-2.769 12.4c.282 11.3-1.749 12.025.37 15.132 8.836 3.65 23.95 19.328 20.39 2.52-2.576-12.171 5.653-21.242-.986-34.742-.54-1.1-1.215-2.165-1.7-3.322l-.638-5.128-10.815 3.641-.485 3.7-2.872 4.943z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 80"
          d="M277.468 300c-4.164-.78-6.09 2.951-8.789 9.04s-7.308 13.486-8.224 14.439-15.443 0-15.443 0-.44 4.206 0 4.708 2.95.44 2.95.44 1 .8 1.946 1.437 8.915 1.827 12.619 1.639 7.094-.5 7.6-1.067 10.609-16.134 11.237-21.721-1.771-8.515-3.896-8.915z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 81"
          d="M265.19 274.386c-7.874.376-6.639 12.222-6.639 12.222s2.045.631 6.33-.075a23.6 23.6 0 01-.189-4.52 29.636 29.636 0 001.13 4.238c.906.415 1.931.047 3.673-.374a7.749 7.749 0 005.153 5.9c1.913.6 3.937 1.318 6.2.188s1.017-12.681-.183-15c-1.8-3.486-4.795-6.607-9.974-4.959a38.433 38.433 0 00-5.501 2.38z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 82"
          d="M278.884 288.454l3.219 4.7s-3.568 8.915 6.809 12.954c8.127 3.163 7.169 8.736 3.5 14.341s2.2 10.5 2.2 10.5.9-6.209 8.791-7.381c10.767-1.6 13.815-16.053 5.774-23.28-6.655-5.981-8.679-1.028-13.184-6.395s-13.007-1.742-13.007-1.742l-3.117-4.545z"
          fill="#0097a9"
        />
        <G data-name="Group 17">
          <Path
            data-name="Path 83"
            d="M312.875 314.85a.286.286 0 01-.251-.424c2.707-4.947.5-10.941-.613-12.565-.118-.172-.23-.347-.348-.531-1.092-1.7-2.45-3.825-9.7-5.13a11.542 11.542 0 01-4.656-2.45c-3.091-2.27-6.6-4.842-14.208-1.338a.286.286 0 01-.239-.52c7.919-3.645 11.567-.967 14.786 1.4a11.132 11.132 0 004.419 2.349c7.495 1.349 8.928 3.586 10.08 5.384.116.181.225.352.338.517 1.164 1.7 3.479 7.981.643 13.163a.286.286 0 01-.251.145z"
            fill="#0097a9"
          />
        </G>
        <Path
          data-name="Path 84"
          d="M250.327 329.8h-88.214a1.642 1.642 0 00-1.642 1.642v.126a1.642 1.642 0 001.642 1.642h13.55l-5.593 61.823h2.116l8.088-61.823h51.382l8.088 61.823h2.116l-5.593-61.823h14.06a1.642 1.642 0 001.642-1.642v-.126a1.642 1.642 0 00-1.642-1.642z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 85"
          d="M324.826 341.934c3.66 3.342 2.068 6.684-.637 12.89s-3.183 29.123-3.183 29.123l2.228-.318s-.318-23.553 2.7-29.919 4.3-9.548 7-7.479-8.108-4.297-8.108-4.297z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 86"
          d="M322.815 337.607c.444-1.981 3.084-4.124 6.614-5.942-.525 2.922-2.207 7.7-.466 8.677 2.349 1.322.229-5.347 1.135-9.012a43.6 43.6 0 0110.975-3.6l-.015.04c-2.069 5.252-4.774 7.321-3.66 8.116.838.6 3.568-5.467 7.172-8.577 2.706-.144 4.995.223 6.349 1.3 2.093 1.657 3.5 3.127 1.393 8.172a9.453 9.453 0 01-4.57 1.657c-4.774.477-7 .636-3.66 2.228 1.636.779 4.379-.044 6.6-.546a82.866 82.866 0 01-2.687 4.548 16.875 16.875 0 01-2.236 2.847 7.065 7.065 0 01-3.591-.006c-2.228-.955-7.957-3.979-9.389-2.706-1.4 1.247 10.61 2.036 9.778 5.2-5.823 3.355-12.485 1.759-13.663-1.322-1.578-4.139-7.418-5.113-6.079-11.074z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 87"
          d="M334.852 360.235c-6.366-4.774-12.891-.318-16.233 4.774s-.955 16.551-.955 16.551 1.273 0 1.273-.477-3.5-13.527 4.774-19.416c5.177-3.683 8.913-1.909 9.39-.159s-2.069 4.934-2.069 4.934z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 88"
          d="M320.21 350.421c6.366 0 8.435 9.867 6.684 15.6s-4.137 16.392-4.137 16.392l-1.592-1.592s6.048-14.959 4.615-21.643-4.933-7.8-5.57-7.8 0-.957 0-.957z"
          fill="#fff"
          stroke="#0097a9"
          strokeMiterlimit={10}
          strokeWidth={0.5}
        />
        <Path
          data-name="Path 89"
          d="M327.29 366.109c12.1-5.092 5.722-12.08 16.385-7.465a7.921 7.921 0 012.583 2.275 7.944 7.944 0 01-3.379.271c-3.182-.477-2.068 2.069 3.183 2.7a11.787 11.787 0 012.461.52c2.5 4.8 3.63 11.128-1.665 13.326a11.979 11.979 0 01-3.768.773c-1.634-1.3.4-8.813-2.12-7.775-2.419.995.757 6.441.322 7.813a34.844 34.844 0 01-7.555-1.059 3.762 3.762 0 01-2-2.3c-.8-3.342 4.456-5.411 4.616-8.276.146-2.633-1.925 5.794-7.413 7.034-4.959-2.311-7.304-5.456-1.65-7.837z"
          fill="#0097a9"
        />
        <Path
          data-name="Path 90"
          d="M318.672 357.105c1.273-2.705-2.069-3.342-1.91-1.273.141 1.833.157 6.29-1.723 7.5a29.5 29.5 0 01-10.989-3.65 1.347 1.347 0 01.14-.829c1.432-2.865 10.662-5.252 7.8-6.207-2.282-.76-6.279 3.729-9.5 5.989-1.935-1.467-3.231-3.217-3.231-5.171 0-2.644 3.212-5.636 7.366-7.067 1.87.189 3.607 1.211 6.32 2.748 4.007 2.271 3.081-1.4-.709-3.58a11.414 11.414 0 018.938 4.906s-.227 1.427 1.283 3.635c4.121 6.022-.635 9.538-6.32 9.31a37.381 37.381 0 012.535-6.311z"
          fill="#0097a9"
        />
        <Path
          data-name="Rectangle 10"
          d="M322.382 394.76h-3.664a5.945 5.945 0 01-5.945-5.945v-9.655h15.554v9.655a5.945 5.945 0 01-5.945 5.945z"
          fill="#0097a9"
        />
        <Path
          data-name="Rectangle 11"
          fill="#fff"
          d="M312.772 380.949h15.555v1.095h-15.555z"
        />
        <Path
          data-name="Rectangle 12"
          fill="#fff"
          d="M312.772 382.946h15.555v.644h-15.555z"
        />
        <Path
          data-name="Path 91"
          d="M50.321 252.424c-3 5.069-7.426 4.569-7.426 4.569s.107-6.712 3.624-7.069 5.587 1.055 6.3 3.384a5.747 5.747 0 010 3.864 6.168 6.168 0 01-1.713-2.321 16.609 16.609 0 01-.785-2.427z"
          fill="#0097a9"
        />
      </G>
    </Svg>
  )
}

export default SvgComponent
