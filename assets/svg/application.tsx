import * as React from "react";
import Svg, { Path, Circle, G, Rect } from "react-native-svg";

interface Props {
    status: string;
    width?: number;
    height?: number;
}

const Selected = () => {
    return ( 
        <G id="Group_233" data-name="Group 233" transform="translate(720.5 -738)">
      <Rect
        id="Rectangle_87"
        data-name="Rectangle 87"
        width={30.002}
        height={17.42}
        rx={3}
        transform="translate(-720 743.172)"
        fill="none"
        stroke="#0097a9"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
      <Path
        id="Path_184"
        data-name="Path 184"
        d="M-702,743.172V740.31a1.811,1.811,0,0,1,1.811-1.81h8.675a1.81,1.81,0,0,1,1.81,1.81v2.862"
        transform="translate(-9.147 0)"
        fill="none"
        stroke="#0097a9"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
      <Circle
        id="Ellipse_11"
        data-name="Ellipse 11"
        cx={3.197}
        cy={3.197}
        r={3.197}
        transform="translate(-708.196 748.921)"
        fill="none"
        stroke="#0097a9"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
    </G>
    );
}

const NotSelected = () => {
    return (
        
        <G id="Group_233" data-name="Group 233" transform="translate(720.5 -738)">
      <Rect
        id="Rectangle_87"
        data-name="Rectangle 87"
        width={30.002}
        height={17.42}
        rx={3}
        transform="translate(-720 743.172)"
        fill="none"
        stroke="#7b7b7b"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
      <Path
        id="Path_184"
        data-name="Path 184"
        d="M-702,743.172V740.31a1.811,1.811,0,0,1,1.811-1.81h8.675a1.81,1.81,0,0,1,1.81,1.81v2.862"
        transform="translate(-9.147 0)"
        fill="none"
        stroke="#7b7b7b"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
      <Circle
        id="Ellipse_11"
        data-name="Ellipse 11"
        cx={3.197}
        cy={3.197}
        r={3.197}
        transform="translate(-708.196 748.921)"
        fill="none"
        stroke="#7b7b7b"
        strokeMiterlimit={10}
        strokeWidth={1}
      />
    </G>
    );
}

const ApplicationSvg = ({status}:Props) => (
  <Svg
    width={31}
    height={26.347}
    viewBox="0 0 31 26.347"
  >
   {status === 'selected' ? <Selected /> : <NotSelected />}
    
  </Svg>
);

export default ApplicationSvg;
